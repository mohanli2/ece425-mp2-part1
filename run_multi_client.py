#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 25 11:53:28 2019

@author: chen
"""

import os
import sys

if __name__ == "__main__":
    interval = 20
    N_node_start = (int(sys.argv[1])-1)*interval
    N_node_end = int(sys.argv[1])*interval
        
       
    num = [str(i) for i in range(N_node_start+1,N_node_end+1)]
    ports = [str(5000+i) for i in range(N_node_start+1,N_node_end+1)] 
    command=[]
    for i in range(interval):
        command.append(f"python36 client.py node{num[i]} {ports[i]}")
        
        
        
    #run
    
    total_comm = ' & \n'.join(command) +' &'
    print(total_comm)
    #file = open("createNode.sh","w")
    ##file.write("#!/bin/bash \n")
    #file.write(total_comm)
    os.system(total_comm)
    
    
    