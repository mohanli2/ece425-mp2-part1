#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 25 21:06:16 2019
analyze logging
@author: chen
"""
import matplotlib.pyplot as plt
import matplotlib
import statistics
def get_log():
    #read server log:
    server_local_time = []
    server_abso_time = []
    server_tx_id = []
    send_time = dict()
    with open("server.log",'r') as file:
        for line in file:
            local_time,abso_time, tx_id,_ ,_,_ = line.split()
            server_local_time.append(local_time)
            server_abso_time.append(abso_time)
            server_tx_id.append(tx_id)
            send_time[tx_id] = abso_time
    #read clients' log
    files = [f"node{i}_transaction.log" for i in range(1,101)]
    
    num_nodes_reached = dict()
    time_used_to_reach = dict()
    #initialize as 0
    for tx in server_tx_id:
        num_nodes_reached[tx] = 0
        time_used_to_reach[tx] = []
    
    #read each file:
    for file_i in files:
        with open(file_i,'r') as file:
            for i,line in enumerate(file):
                local_time,_,abso_time, tx_id,_ ,_,_  = line.split()
                if tx_id in num_nodes_reached:
                    num_nodes_reached[tx_id] += 1
                    time_used_to_reach[tx_id].append(float(abso_time)-float(send_time[tx_id]))
    return server_abso_time,server_tx_id,num_nodes_reached,time_used_to_reach

server_abso_time,server_tx_id,num_nodes_reached,time_used_to_reach = get_log()


font = {'size'   : 16}
matplotlib.rc('font', **font)

plt.figure(figsize = (8,6))
plt.plot(range(1,len(num_nodes_reached)+1),list(num_nodes_reached.values()),'.')
plt.ylabel('Number of nodes reached')
plt.xlabel("Transaction number")
plt.tight_layout()
plt.savefig('F1_100node.png',dpi=300)

#propagation delay
median_time_used_to_reach = [statistics.median(list_) for list_ in time_used_to_reach.values()]
max_time_used_to_reach = [max(list_) for list_ in time_used_to_reach.values()]
min_time_used_to_reach = [min(list_) for list_ in time_used_to_reach.values()]
plt.figure(figsize = (8,6))
plt.plot(range(1,len(num_nodes_reached)+1),median_time_used_to_reach,'r')
plt.plot(range(1,len(num_nodes_reached)+1),min_time_used_to_reach,'g')
plt.plot(range(1,len(num_nodes_reached)+1),max_time_used_to_reach,'b')
plt.legend(['Median','Minimum','Maximum'])
plt.xlabel('Transaction number')
plt.ylabel('Time used (seconds)')
plt.tight_layout()
plt.savefig('F2_100node.png',dpi=300)
          

plt.figure(figsize = (8,6))
plt.plot(range(1,len(num_nodes_reached)+1),median_time_used_to_reach,'r',alpha = 0.7)
plt.plot(range(1,len(num_nodes_reached)+1),min_time_used_to_reach,'g',alpha = 0.7)
plt.plot(range(1,len(num_nodes_reached)+1),max_time_used_to_reach,'b',alpha = 0.7)
plt.legend(['Median','Minimum','Maximum'])
plt.xlabel('Transaction number')
plt.ylabel('Time used (seconds)')
plt.ylim((0,0.1))
plt.tight_layout()
plt.savefig('F3_100node.png',dpi=300)