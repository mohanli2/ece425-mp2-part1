#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import socket
import threading
import time
import re
import logging
import random
import os

class Cryptocurrency(object):
    '''
	1. TCP
	2. Update nodes by sendind and receiving transactions
	3. Sending transactions by gossip
    '''
    def __init__(self, node_name, node_port):
        self.node_IP = socket.gethostbyname(socket.gethostname())
        self.node_name = node_name
        self.node_port = node_port
        self.T = 1 # s

        self.node = set() 
        self.transaction_ID = set()
        self.node_log = self.create_logger(self.node_name + "_connection.log") # log connections between nodes
        self.transaction_log = self.create_logger(self.node_name + "_transaction.log") # log transactions received
	

    def create_logger(self, logname):
        # create logger
        logger = logging.getLogger(logname)
        logger.setLevel(logging.DEBUG)
		# creater file handler
        fh = logging.FileHandler(logname, mode='w')
        fh.setLevel(logging.DEBUG)
        # set file handler format
        formatter = logging.Formatter('%(asctime)s %(message)s', datefmt='%Y:%m:%d:%H:%M:%S')
        fh.setFormatter(formatter)
        logger.addHandler(fh)
        return logger


    def listen_server(self):
        '''
		Join server
		receive message: INTRODUCE, TRANSACTION and DIE from server
        '''
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((server_IP, server_port))
            s.sendall(f"CONNECT {self.node_name} {self.node_IP} {self.node_port}\n".encode())
            while True:
                data = s.recv(1024)
                if not data:
                    break
                #print(data.decode())
                 ### Process data ###
                 

		        # INTRODUCE message: directly store the node, add node_log
                
				#if re.match(r"INTRODUCE\s+(.*)$", data.decode(), re.I):
					#self.process_nd(data.decode())
                if data.decode().startswith('INTRODUCE'):
                    #if more than 1 introduce msg
                    data_ = data.decode().split('\n')
                    for data_i in data_[:-1]:
                        #print(data_i)
                        self.process_nd(data_i)
				# TRANSACTION message: gossip it
                #elif re.match(r"TRANSACTION\s+(.*)$", data.decode(), re.I):
                    #self.gossip_tx(data.decode())
                elif data.decode().startswith('TRANSACTION'):
                    data_ = data.decode().split('\n')
                    for data_i in data_[:-1]:
                        self.gossip_tx(data_i)

				# DIE message: 
                elif re.match(r"DIE\n$", data.decode(), re.I):
					# kill process
                    print("DIE...")
                    os._exit(0)
                else:
                    print(data.decode())
                    print("Unexpected msg from server")

    def gossip_node(self):
        '''
		periodically gossip self 2 known nodes to 3 known nodes, period 4s
		data example: INTRODUCE node 172.22.94.228 4444
        '''
        while True:
            #1. select self, select 2 konwn nodes randomly
            nd_set = set()
            nd_set.add(f"INTRODUCE node {self.node_IP} {self.node_port}")
            known_nd = set(random.sample(self.node, min(2, len(self.node)))) # OR: must send to 3 nodes successfully
            for nd in known_nd:
                nd_set.add(f"INTRODUCE node {nd[0]} {nd[1]}")
            #2. select 3 known nodes randomly
            gossip_node = set(random.sample(self.node, min(3, len(self.node))))
            for i in gossip_node:
                for nd in nd_set:
                    self.send_node(i, nd)
            time.sleep(self.T)

	
    def listen_node(self):
        '''
		listen to other nodes
		receive message: TRANSACTION, INTRODUCE
        '''
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as ss:
            ss.bind((self.node_IP, self.node_port))
            ss.listen(100)
            while True:
                conn, addr = ss.accept() # conn is a socket object
				# No way to know the receive port of other nodes, only know the send port of other nodes
                with conn:
                    while True:
                        data = conn.recv(1024)
                        if not data:
                            break
						### Process data ###
                        if re.match(r"INTRODUCE\s+(.*)$", data.decode(), re.I):
                            self.process_nd(data.decode())
                        elif re.match(r"TRANSACTION\s+(.*)$", data.decode(), re.I):
							# check gossip
                            tx_id = data.decode().split()[2]
                            if tx_id not in self.transaction_ID:
                                self.gossip_tx(data.decode())
                        else:
                            print("Unexpected msg from other nodes")

    def process_nd(self, data):
        '''
		process INTRODUCE msg
        '''
        #print(data)
        node_line = re.search(r"INTRODUCE\s+(.*)$", data, re.I).group(1)
		# node_line example: node1 172.22.156.2 4444
        new_node = (str(node_line.split()[1]), int(node_line.split()[2]))
        if new_node not in self.node:
            self.node.add(new_node)
            self.node_log.info(str(new_node) + " connected")
            print("find node: " + str(new_node))


    def gossip_tx(self, data):
        '''
        data example: TRANSACTION 1553455263.260154 d94ca30d703c289582b6229533c03694 32914 736574 14
        '''
        # 1. record the time when receive message
        tx_time = time.time() 
        tx_id = data.split()[2]
        tx_from = data.split()[3]
        tx_to = data.split()[4]
        tx_amount = data.split()[5]
        tx = f"TRANSACTION {tx_time:#.6f} {tx_id} {tx_from} {tx_to} {tx_amount}"
        # 2. log and record ID
        self.transaction_ID.add(tx_id)
        self.transaction_log.info(tx)
        #print(tx)
        # 3. gossip msg: randomly choose up to three nodes and send msg
        gossip_node = set(random.sample(self.node, min(3, len(self.node)))) # OR: must send to 3 nodes successfully
        for i in gossip_node:
            self.send_node(i, tx)

			
    def send_node(self, node, msg):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            ##### s.bind((self.node_IP, self.node_port)) #####
            # can not bind the msg receive port as the msg send port, have to gossip INTRODUCE msg
            s.connect(node)
            s.sendall(msg.encode())
            #print("Send to node successfully")
        except:
            if node in self.node:
               self.node.remove(node)
               self.node_log.info(str(node) + " disconnected")
               print("remove node: " + str(node))
        s.close()


# main function
if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python36 client.py <node_name> <node_port>")
        sys.exit(1)
    
    server_IP = "172.22.94.228" # run server on vm1, i.e. the first vm
    server_port = 8888
    
    node_name = str(sys.argv[1]) # node1, node2...
    node_port = int(sys.argv[2]) # can not be 8888
    cc = Cryptocurrency(node_name, node_port)
    
    t1 = threading.Thread(target=cc.listen_server)
    t2 = threading.Thread(target=cc.listen_node)
    t3 = threading.Thread(target=cc.gossip_node)
    
    t1.start()
    t2.start()
    t3.start()

'''
summary:
1. only add node in add_nd()
2. only remove node when can be send msg to it
	1). INTRODUCE msg in gossip_node()
	2). TRANSACTION msg in send_node
'''





